DEF_DIR=${DESTDIR}/usr/share/duperemove-service/
SRV_DIR=${DESTDIR}/usr/lib/systemd/system/
RUN_LIB_DIR=${DESTDIR}/usr/lib/duperemove-service/
RUN_BIN_DIR=${DESTDIR}/usr/bin/duperemove-service/

CACHE_DIR=${DESTDIR}/var/cache/duperemove-service/
CONF_DIR=${DESTDIR}/etc/duperemove-service/

ifdef SYSTEMD_SERVICE
	# Install the runner in the lib directory for invoking via the service.
	RUN_DIR=${RUN_LIB_DIR}
else
	# Install the runner in the PATH for invoking by the user or crontab.
	RUN_DIR=${RUN_BIN_DIR}
endif


.PHONY:	install uninstall test

install:
ifndef SKIP_TEST
	./test
else
	$(info Skipping tests)
endif

	# Install the systemd service files.
ifdef SYSTEMD_SERVICE
	mkdir -p ${SRV_DIR}
	cp duperemove@.service ${SRV_DIR}
	cp duperemove@.timer ${SRV_DIR}
endif

	# Install the runner.
	mkdir -p ${RUN_DIR}
	cp duperemove-service ${RUN_DIR}

	# Install the default target configuration.
	mkdir -p ${DEF_DIR}
	cp target-defaults.conf ${DEF_DIR}

	# Create the cache directory.
	mkdir -p ${CACHE_DIR}

	# Create the configuration directory with the inital configuration.
	mkdir -p ${CONF_DIR}
	cp target-defaults.conf ${CONF_DIR}/default.conf


uninstall:
	# Remove the service files if they exist.
	-rm ${SRV_DIR}/duperemove@.service
	-rm  ${SRV_DIR}/duperemove@.timer

	# Remove the runner, wherever it is.
	-rmdir ${RUN_LIB_DIR}
	-rm ${RUN_LIB_DIR}/duperemove-service
	-rm ${RUN_BIN_DIR}/duperemove-service

	# Remove the default target configuration directory.
	rm ${DEF_DIR}/target-defaults.conf
	rmdir ${DEF_DIR}

	# Clean the cache and remove the cache directory.
	rm -f ${CACHE_DIR}/*.db
	rmdir ${CACHE_DIR}

	# Remove the configuration directory if it's empty.
	-rmdir ${CONF_DIR}

test:
	./test
